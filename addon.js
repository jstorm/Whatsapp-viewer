/**
 * Inspired by Loran Kloeze, thanks to him.
 * This script uses known numbers from chat windows,
 * to display their informations on a new sidebar created.
 */

var hasInit;
var GUI_ROOT;

/**
 * Looping trough chat list containing users
 * and create DOM item to show in list
 */
function setupEventListeners() {
    GUI_ROOT.innerHTML = "";
    var users = Store.Chat.models; // all contacts: Store.Contact.models; but whatsapp server is rejecting

    var i,j = 0;
    for(i = 0; i < users.length; i++){
        if(users[i].__x_isUser){
            var clientNr = users[i].__x_id;
            clientNr = clientNr.substr(0,clientNr.length-5);
            clientName = users[i].__x_name;
            if(clientName === undefined)
                clientName = clientNr;
            createClientBox(clientNr, clientName,j);
            j++;
        }
    }
}


/**
 * Calls against Whatsapp API to obtain data
 * @param phonenumber
 * @param name
 * @param count
 */
function createClientBox(phonenumber,name, count) {
    var imgSrcNoneFound = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAIAAAC3LO29AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABDRJREFUeNrsmj1IW1EUgGMNWLBGUIigIlVoq4LUQqsuXerP4uLf4qDSLh1aaRehCk6CDl0q2qFDf6yDi1YXF0272EFLoQWLkQoKGgsKKZggjaCkX97Bx0s01qA2Uc4jhHd/c757zj33nEuSgsGg7UI/l2wX/VFCJVRCJVRCJVRCJVRCJVRCJVRCJVRCJVRCJVRCJVTCUyDcGP3gnXJZi1uzX44e8uvN0PaC+9wQbo6Nux8+MiWmuDU7d/SQ1f7Bf67C6T72k453OH52dN6anIioB3vX57c70lKLi6z1JSPvkx0OXuBMLS7cXliUPtI/JTfncm6OdQZqdjzr9LQbo+JAmH2/FbVge9kP2szKpY5OLDa9ogwAvotevTSblnt608vL8p62zze3ALbn8wU869ITVJBKJyeAtM6w6/OVjAzzHjdPAxuQCGquPcJBhVhojI1K8dCBmdWVt2c+ZTU1oE/ASg1D+D3lkhmM4cN5Tx7H35ciBMu/0tMrRdlmmTVVfKMlPjv78BFPekU53yk52fRBb9gh1ojGWBSKojRnU338CZEsv7vLG1r7RYrJjjS+EVRao+GFz5AWMaFpEccZ/j/OQzTGR6jQDCKu9PQhJbuOSmdTQ0yzZdRUMQNbEXPAjcWTEOtK2Xd9qBG7Emd47Xkfx8bXu/fYVGxI0z1ah9BZVEfR9LfSSn9GBTye1f6BK0ZThJJje4IJ+fxZ88jL+ut3M1evn2Qquy0hnx/NLRybqA5DLejuOslUSYn5TwX2sPgYawxwoQg1t0hUwmjJBza5+mIgEH70SeX5ICT/IAo9IvlgyxH3RRzuUhn/3OJ40XmbGeKcAyvFcjjKP+ffQDPITXF5PyLFCKnEur7V1tGBbmKTnN1bc2HGSbwye/MOH3NsKDl+OySVB42TbtGaTp+QsIOAg5wA6TdHx/HmpE6iorX+QYlXSKnoQM+ljmdibxF5fUZ1JWkH8zDWvCVgaaghjo/IkqEiNiLzoJWmWK8IYiYk2uLnJZMALNOIIY2w242IGKQcX2tGPhWIEjcDzxCCMsm2pLLAiPvIxWi1blfv9EcWbjPkpeaY3HpvciaE32vrvNMuZ2O9qAs8IHEhG2PjyIcELDlhd2pRYVZjfbTTnEm23YssR8QNwKGphlVp/K7kXGdFiPaQj8WW9M/8VeoxJKeBxJYjqbOm/Ac9J8rH5FgRq/8UEpqMfN9hTVwo5j1tl0+syb49xk0Yui/BnRA0hgT1+8VuUd2uz29mvexPxI124yTKxxvthTtY9hjWAR4dWCPJNiXDnm9uxW/JnQ0bMqY7m5ijNnRo7Idcfg8RxcyoNN9DO2fKhSowpx3jDsZsBZulYTlkEsmMJMKGh+VjoJi9qUzRmLyLAR9q2BqXKqESKqESKqESKqESKqESKqESKqESKqESKqESKqESKqHtrwADAMxLRItbnk5RAAAAAElFTkSuQmCC";
    var imgUrl = "";
    var status = "";


    var profilePicRoutine = function(nr) {
        Store.ProfilePicThumb.find( nr + '@c.us').then(function(d){

            if (d.__x_img === null) {
                profilePicRoutine(nr);
            }
            if (d.__x_img === undefined) {
                imgUrl = imgSrcNoneFound;
                statusFindRoutine(phonenumber);
            } else {
                imgUrl = d.__x_img;
                statusFindRoutine(phonenumber);
            }
        }, function(e){
            console.warn(e);
            // Server is throttling/rate limiting, we try it again
            profilePicRoutine(nr);
        });
    };

    var statusFindRoutine = function(nr) {
        Store.Wap.statusFind( nr + '@c.us').then(function(d){
            status = d.status;
            getPresence(nr);
        }, function(e){
            console.warn(e);
            // Server is throttling/rate limiting, we try it again
            statusFindRoutine(nr);
        });
    };

    var getPresence = function(phonenumber)
    {
        Store.Presence.find(phonenumber + '@c.us').then(function (d) {
            if (d.isOnline)
                status = "online";
            else
                status = "offline";
            return GUI_ROOT.appendChild(
                loadListItem(phonenumber, name, status, imgUrl, count));
        });
    };

    profilePicRoutine(phonenumber);
}


/**
 * Check online/offline status every 10 seconds
 */
window.setInterval(function() {
    if(hasInit) {
        console.log("updating status");
        for (var i = 0; i < Store.Presence.models.length; i++) {
            var m = Store.Presence.models[i];
            var id = 'p' + m.__x_id.slice(0, -5);
            var clientBox = document.getElementById(id);
            if (clientBox !== null) {
                var img = clientBox.getElementsByTagName('img')[0];
                img.classList.remove('isOnline');
                var statusText = $("#"+id+" .chat-secondary p");
                if (m.__x_isOnline) {
                    console.log(id +" is now online");
                    clientBox.parentNode.prepend(clientBox);
                    statusText.html("online");
                    img.classList.remove('isOffline');
                    img.classList.add('isOnline');
                } else {
                    img.classList.remove('isOnline');
                    img.classList.add('isOffline');
                    statusText.html("offline");
                }
            }
        }
    } else {
        console.log("cant update interval - no init!");
    }
}, 10000);

/**
 * Initializes the new sidebar and list
 * @returns DOM element node containing the gui root
 */
function initGui(){
    if(hasInit){
        return GUI_ROOT;
    }
    // Let's setup the UI
    // Small delay in case the script is executed from something like Tampermonkey
    var elem = $('div#app > div > div.app.two');
    var dom;
    if(elem+"" !== "null") {
        elem.removeClass("two");
        elem.addClass("three");

        var dom = document.createElement("div");
        dom.className = "pane pane-three";
        dom.style.width = "25%!important";

        //elem has 1 length, but jquery does not support appendchild otherwise
        elem.each( function() {
            this.appendChild(dom);
        });
    }else{
        dom = $('.pane.pane-three');
    }

    var chat_wrapper = document.createElement("div");
    chat_wrapper.className = "chatlist-panel-body";
    chat_wrapper.dataset["listScrollContainer"] = "true";
    chat_wrapper.style.overflow = "auto";

    var div = document.createElement("div");
    div.dataset["tab"] = "3";
    div.tabIndex = "-1";

    var wrapperdiv = document.createElement("div");
    wrapperdiv.className = "chatlist chatlist-main infinite-list";

    var innerdiv = document.createElement("div");
    innerdiv.className = "infinite-list-viewport";
    innerdiv.style = "height: 15840px; pointer-events: auto;";

    dom.appendChild(chat_wrapper);
    chat_wrapper.appendChild(div);
    div.appendChild(wrapperdiv);
    wrapperdiv.appendChild(innerdiv);

    hasInit = true;
    return innerdiv;
}

/**
 * Creates a simple list item as node
 * @param nr
 * @param name
 * @param status
 * @param imgUrl
 * @param position
 * @returns {Element} the DOM element
 */
function loadListItem(nr, name, status, imgUrl, position){
    var root = document.createElement("div");
    root.id = "p"+nr;
    root.className = "infinite-list-item infinite-list-item-transition myHackyListItem";
    root.style = "z-index: 218; height: 72px";

    var tab  = document.createElement("div");
    tab.className = "chat-drag-cover";
    tab.tabIndex = "-1";

    var innertab = document.createElement("div");
    innertab.className = "chat";

    var imageWrapper = document.createElement("div");
    imageWrapper.className = "chat-avatar";

    var bodyWrapper = document.createElement("div");
    bodyWrapper.className = "chat-body";

    var nameWrapper = document.createElement("div");
    nameWrapper.className = "chat-main";

    var onlinestatusWrapper = document.createElement("div");
    onlinestatusWrapper.className = "chat-secondary";

    var p1 = document.createElement("p");
    p1.innerText = name;
    var p2 = document.createElement("p");
    p2.innerText = status;
    var img = document.createElement("img");
    img.src = imgUrl;
    img.href = '#';
    img.addEventListener('click', function(){
        window.open(imgUrl, '_blank');
    });
    img.target = '_blank';

    root.appendChild(tab);
    tab.appendChild(innertab);
    innertab.appendChild(imageWrapper);
    imageWrapper.appendChild(img);
    innertab.appendChild(bodyWrapper);
    bodyWrapper.appendChild(nameWrapper);
    nameWrapper.appendChild(p1);
    bodyWrapper.appendChild(onlinestatusWrapper);
    onlinestatusWrapper.appendChild(p2);

    return root;
}

/**
 * Start Script when every DOM content is loaded and parsed
 */
var timer = setInterval(function() {
    if( typeof $ === "function") {
        //check if dom loaded
        if($('div#app > div > div.app.two').length !== 0) {
            //check for css link tag
            if(document.getElementById("customCSS") !== undefined) {

                clearInterval(timer);
                hasInit = false;
                GUI_ROOT = initGui();

                window.setTimeout(function () {
                    setupEventListeners();
                }, 500);

            } else {
                console.log("CSS hasnt loaded so far");
            }
        }else {
            console.log("DOM not loaded");
        }
    }else {
        console.log("$ still " + typeof $);
    }
},1000);
