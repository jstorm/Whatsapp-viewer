
if(!window.jQuery)
{
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://code.jquery.com/jquery-3.2.1.min.js";
    document.head.appendChild(script);
}

var css  = document.createElement("link");
css.id = "customCSS";
css.rel  = "stylesheet";
css.href  = chrome.extension.getURL("/style/main.css");
document.head.appendChild(css);

var elem  = document.createElement("script");
elem.type = "text/javascript";
elem.src  = chrome.extension.getURL("/addon.js");
(document.head || document.body || document.documentElement).appendChild(elem);
